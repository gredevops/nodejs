var express = require('express')
var app = express()

app.get('/', function (req, res) {
	// body...
	res.send('Hello World from Docker container')
})

app.listen(8081, function () {
	// body...
	console.log('app listening on port 8081 !')
})